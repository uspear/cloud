# 数据统计与分析

开发者通过数据分析接口，可获取到小程序的各项数据指标，便于进行数据存储和整理。

```js
const dailySummaryTrend = await wxa.dailySummaryTrend( begin_date, end_date );//概况趋势
const dailyVisitTrend = await wxa.dailyVisitTrend( begin_date, end_date );//访问日趋势
const weeklyVisitTrend = await wxa.weeklyVisitTrend( begin_date, end_date );//访问周趋势
const monthlyVisitTrend = await wxa.monthlyVisitTrend( begin_date, end_date );//访问月趋势
const visitDistribution = await wxa.visitDistribution( begin_date, end_date );//访问分布
const dailyRetainInfo = await wxa.dailyRetainInfo( begin_date, end_date );//访问日留存
const weeklyRetainInfo = await wxa.weeklyRetainInfo( begin_date, end_date );//访问周留存
const monthlyRetainInfo = await wxa.monthlyRetainInfo( begin_date, end_date );//访问月留存
const visitPage = await wxa.visitPage( begin_date, end_date );//访问页面
const userPortrait = await wxa.userPortrait( begin_date, end_date );//用户画像
```
------

### 概况趋势

用户访问小程序的详细数据可从访问分析中获取，概况中提供累计用户数等部分指标数据。

| 参数   | 是否必填 |   说明   |
| ---- | ------------------ | ---- |
| begin_date   | 是   | 开始日期。例：20171210 |
| end_date   | 是   | 结束日期，限定查询1天数据，end_date允许设置的最大值为昨日。例： 20171210 |

```js
const dailySummaryTrend = await wxa.dailySummaryTrend( begin_date, end_date );
```

返回数据示例：

```js
{
  "list": [
    {
      "ref_date": "20171210",
      "visit_total": 391, //累计用户数
      "share_pv": 572, //转发次数
      "share_uv": 383 //转发人数
    }
  ]
}
```

返回参数说明：

| 参数   | 说明 |
| ---- | ------------------ |
| visit_total   | 累计用户数   |
| share_pv   | 转发次数   |
| share_uv   | 转发人数   |

### 访问趋势

#### 日趋势

| 参数   | 是否必填 |   说明   |
| ---- | ------------------ | ---- |
| begin_date   | 是   | 开始日期。例：20171210 |
| end_date   | 是   | 结束日期，限定查询1天数据，end_date允许设置的最大值为昨日。例： 20171210 |

```js
const dailyVisitTrend = await wxa.dailyVisitTrend( begin_date, end_date );
```

返回数据示例：

```js
{
  "list": [
    {
      "ref_date": "20171210", //时间： 如： "20171210"
      "session_cnt": 142549, //打开次数
      "visit_pv": 472351, //访问次数
      "visit_uv": 55500, //访问人数
      "visit_uv_new": 5464, //新用户数
      "stay_time_session": 0, //次均停留时长 (浮点型，单位：秒)
      "visit_depth": 1.9838 //平均访问深度 (浮点型)
    }
  ]
}
```

返回参数说明：

| 参数 |  说明 |
| ---- | ------------------ |
| ref_date |  时间： 如： "20170313" |
| session_cnt | 打开次数 |
| visit_pv |  访问次数 |
| visit_uv |  访问人数 |
| visit_uv_new |  新用户数 |
| stay_time_uv |  人均停留时长 (浮点型，单位：秒) |
| stay_time_session | 次均停留时长 (浮点型，单位：秒) |
| visit_depth | 平均访问深度 (浮点型) |

#### 周趋势

| 参数   | 是否必填 |   说明   |
| ---- | ------------------ | ---- |
| begin_date   | 是   | 开始日期，为周一日期 |
| end_date   | 是   | 结束日期，为周日日期，限定查询一周数据 |

注意：请求json和返回json与天的一致，这里限定查询一个自然周的数据，时间必须按照自然周的方式输入： 如：20170306(周一), 20170312(周日)

```js
const weeklyVisitTrend = await wxa.weeklyVisitTrend( begin_date, end_date );
```

返回数据示例：

```js
{
  "list": [
    {
      "ref_date": "20170306-20170312", //时间，如："20170306-20170312"
      "session_cnt": 986780, //打开次数（自然周内汇总）
      "visit_pv": 3251840, //访问次数（自然周内汇总）
      "visit_uv": 189405, //访问人数（自然周内去重）
      "visit_uv_new": 45592, //新用户数（自然周内去重）
      "stay_time_session": 54.5346, //次均停留时长 (浮点型，单位：秒)
      "visit_depth": 1.9735 //平均访问深度 (浮点型)
    }
  ]
}
```

返回参数说明：

| 参数 |  说明 |
| ---- | ------------------ |
| ref_date |  时间，如："20170306-20170312" |
| session_cnt | 打开次数（自然周内汇总）|
| visit_pv |  访问次数（自然周内汇总）|
| visit_uv |  访问人数（自然周内去重）|
| visit_uv_new |  新用户数（自然周内去重）|
| stay_time_uv |  人均停留时长 (浮点型，单位：秒) |
| stay_time_session | 次均停留时长 (浮点型，单位：秒) |
| visit_depth | 平均访问深度 (浮点型) |

#### 月趋势

| 参数   | 是否必填 |   说明   |
| ---- | ------------------ | ---- |
| begin_date   | 是   | 开始日期，为自然月第一天 |
| end_date   | 是   | 结束日期，为自然月最后一天，限定查询一个月数据 |

注意：请求json和返回json与天的一致，这里限定查询一个自然月的数据，时间必须按照自然月的方式输入： 如：20170201(月初), 20170228(月末)

```js
const monthlyVisitTrend = await wxa.monthlyVisitTrend( begin_date, end_date );
```

返回数据示例：

```js
{
  "list": [
    {
      "ref_date": "201702", //时间，如："201702"
      "session_cnt": 126513, //打开次数（自然月内汇总）
      "visit_pv": 426113, //访问次数（自然月内汇总）
      "visit_uv": 48659, //访问人数（自然月内去重）
      "visit_uv_new": 6726, //新用户数（自然月内去重）
      "stay_time_session": 56.4112, //人均停留时长 (浮点型，单位：秒)
      "visit_depth": 2.0189 //平均访问深度 (浮点型)
    }
  ]
}
```

返回参数说明：

| 参数 |  说明 |
| ---- | ------------------ |
| ref_date |  时间，如："201702" |
| session_cnt | 打开次数（自然月内汇总）|
| visit_pv |  访问次数（自然月内汇总）|
| visit_uv |  访问人数（自然月内去重）|
| visit_uv_new |  新用户数（自然月内去重）|
| stay_time_uv |  人均停留时长 (浮点型，单位：秒) |
| stay_time_session | 次均停留时长 (浮点型，单位：秒) |
| visit_depth | 平均访问深度 (浮点型) |

### 访问分布

| 参数   | 是否必填 |   说明   |
| ---- | ------------------ | ---- |
| begin_date   | 是   | 开始日期，为自然月第一天。例：20171210 |
| end_date   | 是   | 结束日期，限定查询1天数据，end_date允许设置的最大值为昨日。例：20171210 |

```js
const visitDistribution = await wxa.visitDistribution( begin_date, end_date );
```

返回数据示例：

```js
{
  "ref_date": "20171210",
  "list": [
    {
      "index": "access_source_session_cnt",
      "item_list": [
        {
          "key": 10,
          "value": 5
        },
        {
          "key": 8,
          "value": 687
        },
        {
          "key": 7,
          "value": 10740
        },
        {
          "key": 6,
          "value": 1961
        },
        {
          "key": 5,
          "value": 677
        },
        {
          "key": 4,
          "value": 653
        },
        {
          "key": 3,
          "value": 1120
        },
        {
          "key": 2,
          "value": 10243
        },
        {
          "key": 1,
          "value": 116578
        }
      ]
    },
    {
      "index": "access_staytime_info",
      "item_list": [
        {
          "key": 8,
          "value": 16329
        },
        {
          "key": 7,
          "value": 19322
        },
        {
          "key": 6,
          "value": 21832
        },
        {
          "key": 5,
          "value": 19539
        },
        {
          "key": 4,
          "value": 29670
        },
        {
          "key": 3,
          "value": 19667
        },
        {
          "key": 2,
          "value": 11794
        },
        {
          "key": 1,
          "value": 4511
        }
      ]
    },
    {
      "index": "access_depth_info",
      "item_list": [
        {
          "key": 5,
          "value": 217
        },
        {
          "key": 4,
          "value": 3259
        },
        {
          "key": 3,
          "value": 32445
        },
        {
          "key": 2,
          "value": 63542
        },
        {
          "key": 1,
          "value": 43201
        }
      ]
    }
  ]
}
```

返回参数说明：

| 参数   | 说明 |
| ---- | ------------------ |
| ref_date |  时间： 如： "20170313" |
| list |  存入所有类型的指标情况 |

list 的每一项包括：

| 参数   | 说明 |
| ---- | ------------------ |
| index | 分布类型 |
| item_list | 分布数据列表 |

分布类型（index）的取值范围：

| 值  | 说明 |
| ---- | ------------------ |
| access_source_session_cnt | 访问来源分布 |
| access_staytime_info |  访问时长分布 |
| access_depth_info | 访问深度的分布 |

每个数据项包括：

| 参数 |  说明 |
| ---- | ------------------ |
| key | 场景 id |
| value | 场景下的值（均为整数型）|

key对应关系如下：

访问来源：(index="access_source_session_cnt")

  1：小程序历史列表

  2：搜索

  3：会话

  4：二维码

  5：公众号主页

  6：聊天顶部

  7：系统桌面

  8：小程序主页

  9：附近的小程序

  10：其他

  11：模板消息

  12：客服消息

  13: 公众号菜单

  14: APP分享

  15: 支付完成页

  16: 长按识别二维码

  17: 相册选取二维码

  18: 公众号文章

  19：钱包

  20：卡包

  21：小程序内卡券

  22：其他小程序

  23：其他小程序返回

  24：卡券适用门店列表

  25：搜索框快捷入口

  26：小程序客服消息

  27：公众号下发

访问时长：(index="access_staytime_info")

  1: 0-2s

  2: 3-5s

  3: 6-10s

  4: 11-20s

  5: 20-30s

  6: 30-50s

  7: 50-100s

  8: > 100s

平均访问深度：(index="access_depth_info")

  1: 1页

  2: 2页

  3: 3页

  4: 4页

  5: 5页

  6: 6-10页

  7: >10页

### 访问留存

#### 日留存

| 参数   | 是否必填 |   说明   |
| ---- | ------------------ | ---- |
| begin_date   | 是   | 开始日期。例：20171210 |
| end_date   | 是   | 结束日期，限定查询1天数据，end_date允许设置的最大值为昨日。例：20171210 |

```js
const dailyRetainInfo = await wxa.dailyRetainInfo( begin_date, end_date );
```

返回数据示例：

```js
{
  "ref_date": "20171210",
  "visit_uv_new": [
    {
      "key": 0,
      "value": 5464
    }
  ],
  "visit_uv": [
    {
      "key": 0,
      "value": 55500
    }
  ]
}
```

返回参数说明：

| 参数 |  说明 |
| ---- | ------------------ | ---- |
| visit_uv_new |  新增用户留存 |
| visit_uv |  活跃用户留存 |

visit_uv、visit_uv_new 的每一项包括:

| 参数 |  说明 |
| ---- | ------------------ | ---- |
| key | 标识，0开始，0表示当天，1表示1天后，依此类推，key取值分别是：0,1,2,3,4,5,6,7,14,30 |
| value | key对应日期的新增用户数/活跃用户数（key=0时）或留存用户数（k>0时）|

#### 周留存

| 参数   | 是否必填 |   说明   |
| ---- | ------------------ | ---- |
| begin_date   | 是   | 开始日期，为周一日期。例：20170306 |
| end_date   | 是   | 结束日期，为周日日期，限定查询一周数据。例：20170312 |

注意：请求json和返回json与天的一致，这里限定查询一个自然周的数据，时间必须按照自然周的方式输入： 如：20170306(周一), 20170312(周日)

```js
const weeklyRetainInfo = await wxa.weeklyRetainInfo( begin_date, end_date );
```

返回数据示例：

```js
{
  "ref_date": "20170306-20170312",
  "visit_uv_new": [
    {
      "key": 0,
      "value": 0
    },
    {
      "key": 1,
      "value": 16853
    }
  ],
  "visit_uv": [
    {
      "key": 0,
      "value": 0
    },
    {
      "key": 1,
      "value": 99310
    }
  ]
}
```

返回参数说明：

| 参数 |  说明 |
| ---- | ------------------ | ---- |
| ref_date |  时间，如："20170306-20170312" |
| visit_uv_new |  新增用户留存 |
| visit_uv |  活跃用户留存 |

visit_uv、visit_uv_new 的每一项包括:

| 参数 |  说明 |
| ---- | ------------------ | ---- |
| key | 标识，0开始，0表示当周，1表示1周后，依此类推，key取值分别是：0,1,2,3,4 |
| value | key对应日期的新增用户数/活跃用户数（key=0时）或留存用户数（k>0时） |

#### 月留存

| 参数   | 是否必填 |   说明   |
| ---- | ------------------ | ---- |
| begin_date   | 是   | 开始日期，为自然月第一天。例：20170201 |
| end_date   | 是   | 结束日期，为自然月最后一天，限定查询一个月数据。例：20170228 |

注意：请求json和返回json与天的一致，这里限定查询一个自然月的数据，时间必须按照自然月的方式输入： 如：20170201(月初), 20170228(月末)

```js
const monthlyRetainInfo = await wxa.monthlyRetainInfo( begin_date, end_date );
```

返回数据示例：

```js
{
  "ref_date": "201702",
  "visit_uv_new": [
    {
      "key": 0,
      "value": 346249
    }
  ],
  "visit_uv": [
    {
      "key": 0,
      "value": 346249
    }
  ]
}
```

返回参数说明：

| 参数 |  说明 |
| ---- | ------------------ | ---- |
| ref_date |  时间，如："201702" |
| visit_uv_new |  新增用户留存 |
| visit_uv |  活跃用户留存 |

visit_uv、visit_uv_new 的每一项包括:

| 参数 |  说明 |
| ---- | ------------------ | ---- |
| key | 标识，0开始，0表示当月，1表示1月后，key取值分别是：0,1 |
| value | key对应日期的新增用户数/活跃用户数（key=0时）或留存用户数（k>0时）|

### 访问页面

| 参数   | 是否必填 |   说明   |
| ---- | ------------------ | ---- |
| begin_date   | 是   | 开始日期。例：20170313 |
| end_date   | 是   | 结束日期，限定查询1天数据，end_date允许设置的最大值为昨日。例：20170313 |

```js
const visitPage = await wxa.visitPage( begin_date, end_date );
```

返回数据示例：

```js
{
  "ref_date": "20170313",
  "list": [
    {
      "page_path": "pages/main/main.html",
      "page_visit_pv": 213429,
      "page_visit_uv": 55423,
      "page_staytime_pv": 8.139198,
      "entrypage_pv": 117922,
      "exitpage_pv": 61304,
      "page_share_pv": 180,
      "page_share_uv": 166
    },
    {
      "page_path": "pages/linedetail/linedetail.html",
      "page_visit_pv": 155030,
      "page_visit_uv": 42195,
      "page_staytime_pv": 35.462395,
      "entrypage_pv": 21101,
      "exitpage_pv": 47051,
      "page_share_pv": 47,
      "page_share_uv": 42
    },
    {
      "page_path": "pages/search/search.html",
      "page_visit_pv": 65011,
      "page_visit_uv": 24716,
      "page_staytime_pv": 6.889634,
      "entrypage_pv": 1811,
      "exitpage_pv": 3198,
      "page_share_pv": 0,
      "page_share_uv": 0
    },
    {
      "page_path": "pages/stationdetail/stationdetail.html",
      "page_visit_pv": 29953,
      "page_visit_uv": 9695,
      "page_staytime_pv": 7.558508,
      "entrypage_pv": 1386,
      "exitpage_pv": 2285,
      "page_share_pv": 0,
      "page_share_uv": 0
    },
    {
      "page_path": "pages/switch-city/switch-city.html",
      "page_visit_pv": 8928,
      "page_visit_uv": 4017,
      "page_staytime_pv": 9.22659,
      "entrypage_pv": 748,
      "exitpage_pv": 1613,
      "page_share_pv": 0,
      "page_share_uv": 0
    }
  ]
}
```

返回参数说明：

| 参数 |  说明 |
| ---- | ------------------ | ---- |
| page_path | 页面路径 |
| page_visit_pv | 访问次数 |
| page_visit_uv | 访问人数 |
| page_staytime_pv |  次均停留时长 |
| entrypage_pv |  进入页次数 |
| exitpage_pv | 退出页次数 |
| page_share_pv | 转发次数 |
| page_share_uv | 转发人数 |

注意：目前只提供按 page_visit_pv 排序的 top200

### 用户画像

获取小程序新增或活跃用户的画像分布数据。时间范围支持昨天、最近7天、最近30天。其中，新增用户数为时间范围内首次访问小程序的去重用户数，活跃用户数为时间范围内访问过小程序的去重用户数。画像属性包括用户年龄、性别、省份、城市、终端类型、机型。

| 参数   | 是否必填 |   说明   |
| ---- | ------------------ | ---- |
| begin_date   | 是   | 开始日期。例：2017-12-18 |
| end_date   | 是   | 结束日期，开始日期与结束日期相差的天数限定为0/6/29，分别表示查询最近1/7/30天数据，end_date允许设置的最大值为昨日。例：2017-12-24 |

```js
const userPortrait = await wxa.userPortrait( begin_date, end_date );
```

返回数据示例：

```js
{
  "ref_date": "20170611",
  "visit_uv_new": {
    "province": [
      {
        "id": 31,
        "name": "广东省",
        "value": 215
      }
    ],
    "city": [
     {
        "id": 3102,
        "name": "广州",
        "value": 78
      }
    ],
    "genders": [
      {
        "id": 1,
        "name": "男",
        "value": 2146
      }
    ],
    "platforms": [
      {
        "id": 1,
        "name": "iPhone",
        "value": 27642
      }
    ],
    "devices": [
      {
        "name": "OPPO R9",
        "value": 61
      }
    ],
    "ages": [
      {
        "id": 1,
        "name": "17岁以下",
        "value": 151
      }
    ]
  },
  "visit_uv": {
    "province": [
      {
        "id": 31,
        "name": "广东省",
        "value": 1341
      }
    ],
    "city": [
     {
        "id": 3102,
        "name": "广州",
        "value": 234
      }
    ],
    "genders": [
      {
        "id": 1,
        "name": "男",
        "value": 14534
      }
    ],
    "platforms": [
      {
        "id": 1,
        "name": "iPhone",
        "value": 21750
      }
    ],
    "devices": [
      {
        "name": "OPPO R9",
        "value": 617
      }
    ],
    "ages": [
      {
        "id": 1,
        "name": "17岁以下",
        "value": 3156
      }
    ]
  }
}
```

返回参数说明：

每次请求返回选定的时间范围及以下指标项：

| 参数 |  说明 |
| ---- | ------------------ | ---- |
| ref_date |  时间范围,如： "20170611-20170617" |
| visit_uv_new |  新用户 |
| visit_uv |  活跃用户 |

每个指标项下包括的属性：

| 参数 |  说明 |
| ---- | ------------------ | ---- |
| province |  省份，如北京、广东等 |
| city |  城市，如北京、广州等 |
| genders | 性别，包括男、女、未知 |
| platforms | 终端类型，包括iPhone, android,其他 |
| devices | 机型，如苹果iPhone6, OPPO R9等 |
| ages |  年龄，包括17岁以下、18-24岁等区间 |

每个属性下包括的数据项：

| 参数 |  说明 |
| ---- | ------------------ | ---- |
| id |  属性值id |
| name |  属性值名称，与id一一对应。如属性为province时，返回的属性值名称包括“广东”等 |
| value | 属性值对应的指标值，如指标为visit_uv,属性为province,属性值为"广东省”，value对应广东地区的活跃用户数 |

注：由于部分用户属性数据缺失，属性值可能出现 “未知”。机型数据无 id 字段，暂只提供用户数最多的 top20。